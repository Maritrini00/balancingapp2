# Pŕactica Balanceo de Cargas

329675 Velázquez Ruiz Maritrini



# ¿Cómo funciona?

Para poder ejecutar esta práctica solo debemos realizar los siguientes pasos:
1. Clonar el repositorio.
2. Entrar a la carpeta raíz del proyecto y ejecutar el proyecto con el comando:

				docker-compose up
3. Entrar al navegador en el localhost ya sea en el puerto 3000, 3001 y 3002.
4. Como resultado renderizará el numero que hayamos pasado del a siguiente manera: "La instancia ejecutada es la numero 1".



